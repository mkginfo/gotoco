/*
 * (C) 2014 Michael Engelhardt <me@mindcrime-ilab.de>
 *
 * This file is part of Goto.Co.
 *
 * Goto.Co is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Goto.Co is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Goto.Co.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

var app = angular.module('gotoco.controllers');

app.controller('SessionsCtrl', function ($scope, $log, $ionicLoading, CacheService) {
  $log.debug('scope session:' + $scope.sessions);

  $scope.clear = function () {
    $log.debug('Resetting search scope');
    $scope.search = undefined;
  };

  function load() {
    $log.debug('Showing progress img');
    $ionicLoading.show({
      template: '<div class="loader"><i class="icon ion-loading-c"></i><br /><span>Loading...</span></div>'
    });

    $log.debug('defining sessions list');
    $scope.sessions = [];

    CacheService.queryForSessions().$promise
      .then(function (data) {


        var sessions = data.result.allSessions
          .map(function (session) {
            return session.id;
          })
          .filter(function (value, index, self) {
            return self.indexOf(value) === index;
          });


        $log.debug('Number of sessions: ' + sessions.length);

        angular.forEach(sessions, function (session, idx) {
          if ((idx + 1) === sessions.length) {
            loadSessions(session, function () {
              $ionicLoading.hide();
              $log.debug('progress screen hidden');
            });
          }
          else {
            loadSessions(session);
          }
        });
      })
      .catch(function (err) {
        $log.error('Could not fetch session data: ' + err);
      });
  }

  function loadSessions(id, func) {
    //$log.debug('Loading session for id: '+id);
    CacheService.getSession({sessionId: id}).$promise
      .then(function (data) {
        //$log.debug('session loaded for id: '+id);
        if (typeof(data.room) !== 'undefined' && data.room !== null) {
          $scope.sessions.push(data);
        }
      })
      .catch(function (err) {
        $log.error('Could not fetch detail session information' + err);
      })
      .finally(function () {
        if (typeof(func) === 'function') {
          func();
        }
      });
  }

  if (typeof($scope.sessions) === 'undefined') {
    load();
  }
});
