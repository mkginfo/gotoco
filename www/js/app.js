/*
 * (C) 2014 Michael Engelhardt <me@mindcrime-ilab.de>
 *
 * This file is part of Goto.Co.
 *
 * Goto.Co is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Goto.Co is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Goto.Co.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('gotoco', [
  'ionic',
  'ngResource',
  'ngSanitize',
  'gotoco.services',
  'gotoco.controllers',
  'gotoco.directives'
]);

app.constant('CONFERENCE_ID', '2f44b3b7308f4a0c8cf450dbb7e5eb97');

app.constant('DATABASE', {
  name: 'gotoconf2014',
  version: 1,
  stores: {
    sessions: 'Sessions',
    tracks: 'Tracks',
    speakers: 'Speakers',
    mySessions: 'MySessions'
  }
});

app.config(function ($logProvider) {
  $logProvider.debugEnabled(false);
});


app.filter('asDate', function () {
  return function (input) {
    return new Date(input);
  };
});

app.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      window.StatusBar.styleDefault();
    }
  });
});

app.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html'
    })

    .state('app.tracks', {
      url: '/tracks',
      views: {
        'menuContent': {
          templateUrl: 'templates/tracks.html',
          controller: 'TracksCtrl'
        }
      }
    })

    .state('app.track', {
      url: '/track/:trackId',
      views: {
        'menuContent': {
          templateUrl: 'templates/track.html',
          controller: 'TrackCtrl'
        }
      }
    })

    .state('app.settings', {
      url: '/settings',
      views: {
        'menuContent': {
          templateUrl: 'templates/settings.html',
          controller: 'SettingsCtrl'
        }
      }
    })

    .state('app.sessions', {
      url: '/sessions',
      views: {
        'menuContent': {
          templateUrl: 'templates/sessions.html',
          controller: 'SessionsCtrl'
        }
      }
    })

    .state('app.session', {
      url: '/session/:sessionId',
      views: {
        'menuContent': {
          templateUrl: 'templates/session.html',
          controller: 'SessionCtrl'
        }
      }
    })

    .state('app.speaker', {
      url: '/speaker/:speakerId',
      views: {
        'menuContent': {
          templateUrl: 'templates/speaker.html',
          controller: 'SpeakerCtrl'
        }
      }
    })

    .state('app.myschedule', {
      url: '/myschedule',
      views: {
        'menuContent': {
          templateUrl: 'templates/myschedule.html',
          controller: 'MyScheduleCtrl'
        }
      }
    })

    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    });
// if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/sessions');
})
;

/**
 * Firefox OS only
 * @see http://goo.gl/cKJyRm
 *
 app.config(['$compileProvider', function($compileProvider) {
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|app):/);
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|app):/);
}
 ]);
 */
