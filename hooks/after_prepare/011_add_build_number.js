#!/usr/bin/env node
// Based on the 'replace text' hook found here: http://devgirl.org/2013/11/12/three-hooks-your-cordovaphonegap-project-needs/
// This hook should be placed in the 'after_prepare' hook folder.

// The hook relies on a JSON file located at '<project_root>/resources/.build.json' to track the build number.
// build.json content:
// {'build: 1}

// Add 'BUILDNR' to the version number in the '<project_root>/config.xml' file.

// this plugin replaces arbitrary text in arbitrary files
//
// Look for the string CONFIGURE HERE for areas that need configuration
//

var fs = require('fs');
var path = require('path');

var rootdir = process.argv[2];

function replace_string_in_file(filename, to_replace, replace_with) {
  var data = fs.readFileSync(filename, 'utf8');

  var result = data.replace(new RegExp(to_replace, 'g'), replace_with);
  fs.writeFileSync(filename, result, 'utf8');
}

process.stdout.write('Target ['+process.env.TARGET+'], rootdir['+rootdir+']\n');

var target = 'stage';
if (process.env.TARGET) {
  target = process.env.TARGET;
}

if (rootdir) {
  var buildfile = path.join(rootdir, 'resources', 'build.json');
  var buildobj = JSON.parse(fs.readFileSync(buildfile, 'utf8'));

  process.stdout.write('bumb version to '+buildobj.version+'-'+buildobj.build+'\n');

  // CONFIGURE HERE
  // with the names of the files that contain tokens you want replaced.  Replace files that have been copied via the prepare step.

  var buildstoreplace = [
    //ios
    //'platforms/ios/YouLinker/config.xml',
    //'platforms/ios/YouLinker/YouLinker-info.plist',

    //android
    'platforms/android/AndroidManifest.xml',
    'platforms/android/ant-build/AndroidManifest.cordova.xml',
    'platforms/android/res/xml/config.xml',
    'platforms/android/assets/www/templates/menu.html',

    //firefoxos
    'platforms/firefoxos/config.xml',
    'platforms/firefoxos/www/manifest.webapp',
    'platforms/firefoxos/www/templates/menu.html'

  ];

  buildstoreplace.forEach(function(val, index, array) {
    'use strict';
    var fullfilename = path.join(rootdir, val);

    if (fs.existsSync(fullfilename)) {
      replace_string_in_file(fullfilename, 'BUILDNR', buildobj.build);
      replace_string_in_file(fullfilename, 'VERSIONNR', buildobj.version);
      // ... any other configuration
    } else {
      //console.log('missing: '+fullfilename);
    }
  });
  console.log('Build number updated to ' + buildobj.build);
  buildobj.build++;
  fs.writeFileSync(buildfile, JSON.stringify(buildobj), ['utf8']);

}
